#!/bin/sh
set -e

if [ "$1" = 'default' ]; then
    exec /bin/sh -c "dd if=/dev/zero count=1 of=created-file bs=1M"
else
	exec /bin/sh -c "dd if=/dev/zero count=1 of=created-file bs=$1"
fi

exec "$@"