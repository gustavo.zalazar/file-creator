# Docker file-creator

## clonar

```sh
mkdir -p Workspace/afip/
cd Workspace/afip/
git clone https://gitlab.com/gustavo.zalazar/file-creator.git
```

## build

```sh
cd Workspace/afip/file-creator
```

## execute

```sh
docker run mikroways/file-creator 
```

## test

docker ps -asn 2